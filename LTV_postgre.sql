-- LTV через регрессию, запускается в синтаксисе постгре
-- общее количество юзеров в когорте
with cohort as (
    select count(distinct concat(idfa, ',', idfv)) as total_users_of_cohort
    from silver.events_with_installs
),
-- количество активных юзеров на 1ый день, 2ой, 3ий и тд. из когорты
active_users as (
    select date_part('day', event_date - install_date) as activity_day,
               count(distinct concat(idfa, ',', idfv)) as active_users_of_day
    from silver.events_with_installs
    group by 1
    --берем только первые 90 дней, остальные дни предсказываем
    having date_part('day', event_date - install_date) between 1 and 90
),
-- рассчитываем коэффициенты регрессии
coef as (
    select exp(regr_intercept(ln(activity), ln(activity_day))) as a,
                regr_slope(ln(activity), ln(activity_day)) as b
    from(
                select activity_day,
                       active_users_of_day / total_users_of_cohort as activity
                from active_users
                cross join cohort order by activity_day
            )
),
lt as(
select generate_series as activity_day,
           active_users_of_day::real / total_users_of_cohort as real_data,
           a*power(generate_series,b) as pred_data,
           sum(a*power(generate_series,b)) over(order by generate_series) as cumulative_lt
from generate_series(1,180,1)
cross join coef
join active_users on generate_series = activity_day::int
)
select cumulative_lt as LT,
           cumulative_lt * 83.7 as LTV
from lt;
