-- retention
SELECT cohort.store_id, cohort.platform, cohort.country_code,
       cohort.date_diff AS day_difference,
       avg(cohort.ret_base)/avg(reg.users)*100 AS retention_rate
FROM
  (SELECT store_id, platform, country_code,
       install_date AS reg_date,
       count(distinct concat(idfa, ',', idfv)) AS users
FROM silver.events_with_installs
WHERE install_date <= date_add(now(), -30) and -- if you need retention more that 30 days you need decrease -30
      store_id in ('1497100207', 'com.trickybricky') and -- games which need to check
      platform = 'android' -- platform
GROUP BY 1,2,3,4) reg
LEFT JOIN
  (SELECT store_id, platform, country_code,
       install_date AS reg_date,
          datediff(event_date, install_date) AS date_diff,
          count(distinct concat(idfa, ',', idfv)) AS ret_base
FROM silver.events_with_installs
WHERE 1=1
     AND datediff(event_date, install_date) between 0 and 30
     AND install_date <= date_add(now(), -30) and -- if you need retention more that 30 days you need decrease -30
      store_id in ('1497100207', 'com.trickybricky') and -- games which need to check
      platform = 'android' -- platform
GROUP BY 1, 2,3,4,5) cohort ON cohort.store_id = reg.store_id and cohort.platform = reg.platform and cohort.country_code = reg.country_code and reg.reg_date=cohort.reg_date
    GROUP BY 1,2,3,4
order by 5 desc;